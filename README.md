# stockboy-mobile

# INSTALL
No installation is required for this project, simply open the register.html page to begin using it.

# LICENSE
I chose the MIT license because it lets people do almost anything they want with my project, including making and distributing closed source versions. Other projects that use the MIT license include Babel, .NET Core, and Rails.
